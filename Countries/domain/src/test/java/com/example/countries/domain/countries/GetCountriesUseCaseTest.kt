package com.example.countries.domain.countries

import com.example.countries.domain.country.CountryRepository
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetCountriesUseCaseTest {
    @Mock
    private lateinit var countryRepository: CountryRepository

    private lateinit var getCountriesUseCase: GetCountriesUseCase

    @Before
    fun setUp() {
        reset(countryRepository)
        getCountriesUseCase = GetCountriesUseCase(countryRepository)
    }

    @Test
    fun `test successful get countries`() {
        whenever(countryRepository.getCountries()).thenReturn(Single.just(listOf(mock())))

        getCountriesUseCase.run().test()
                .assertNoErrors()
                .assertComplete()
                .assertValueCount(1)

        verify(countryRepository).getCountries()
        verifyNoMoreInteractions(countryRepository)
    }

    @Test
    fun `test failure get countries`() {
        whenever(countryRepository.getCountries()).thenReturn(Single.error(NullPointerException()))

        getCountriesUseCase.run().test()
                .assertError(NullPointerException::class.java)
                .assertNotComplete()
                .assertNoValues()

        verify(countryRepository).getCountries()
        verifyNoMoreInteractions(countryRepository)
    }
}