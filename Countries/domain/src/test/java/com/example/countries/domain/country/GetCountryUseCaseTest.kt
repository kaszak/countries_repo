package com.example.countries.domain.country

import com.nhaarman.mockito_kotlin.*
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetCountryUseCaseTest {
    @Mock
    private lateinit var countryRepository: CountryRepository

    private lateinit var getCountryUseCase: GetCountryUseCase

    @Before
    fun setUp() {
        reset(countryRepository)
        getCountryUseCase = GetCountryUseCase(countryRepository)
    }

    @Test
    fun `test successful get countries`() {
        whenever(countryRepository.getCountry(anyInt())).thenReturn(Single.just(mock()))

        getCountryUseCase.run(anyInt()).test()
                .assertNoErrors()
                .assertComplete()
                .assertValueCount(1)

        verify(countryRepository).getCountry(anyInt())
        verifyNoMoreInteractions(countryRepository)
    }

    @Test
    fun `test failure get countries`() {
        whenever(countryRepository.getCountry(anyInt())).thenReturn(Single.error(NullPointerException()))

        getCountryUseCase.run(anyInt()).test()
                .assertError(NullPointerException::class.java)
                .assertNotComplete()
                .assertNoValues()

        verify(countryRepository).getCountry(anyInt())
        verifyNoMoreInteractions(countryRepository)
    }
}