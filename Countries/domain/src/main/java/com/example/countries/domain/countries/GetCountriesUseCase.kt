package com.example.countries.domain.countries

import com.example.countries.domain.SingleNoParamUseCase
import com.example.countries.domain.country.Country
import com.example.countries.domain.country.CountryRepository
import io.reactivex.Single
import javax.inject.Inject

class GetCountriesUseCase @Inject constructor(private val countryRepository: CountryRepository) : SingleNoParamUseCase<List<Country>> {
    override fun run(): Single<List<Country>> {
        return countryRepository.getCountries()
    }
}