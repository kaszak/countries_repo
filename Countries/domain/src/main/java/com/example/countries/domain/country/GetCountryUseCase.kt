package com.example.countries.domain.country

import com.example.countries.domain.SingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class GetCountryUseCase @Inject constructor(private val countryRepository: CountryRepository) : SingleUseCase<Int, CountryExtended> {
    override fun run(input: Int): Single<CountryExtended> {
        return countryRepository.getCountry(input)
    }
}