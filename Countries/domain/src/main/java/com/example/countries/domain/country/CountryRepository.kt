package com.example.countries.domain.country

import io.reactivex.Single

interface CountryRepository {
    fun getCountry(countryId: Int): Single<CountryExtended>
    fun getCountries(): Single<List<Country>>
}