package com.example.countries.domain.country

data class CountryExtended(val country: Country,
                           val description: String = "",
                           val seeMoreUrl: String = "")