package com.example.countries.domain.country

import java.util.Date

data class Country(val id: Int = -1,
                   val pictureUrl: String = "",
                   val name: String = "",
                   val date: Date = Date())