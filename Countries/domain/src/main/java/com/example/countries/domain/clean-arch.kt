package com.example.countries.domain

import io.reactivex.Single

interface SingleNoParamUseCase<Output> {
    fun run(): Single<Output>
}

interface SingleUseCase<in Input, Output> {
    fun run(input: Input): Single<Output>
}

interface Mapper<in From, out To> {
    fun map(obj: From): To
}