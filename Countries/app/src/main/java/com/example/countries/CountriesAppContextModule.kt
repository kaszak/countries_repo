package com.example.countries

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class CountriesAppContextModule {

    @Provides
    @Singleton
    fun provideApplication(app: CountriesApp): Application = app

    @Provides
    @Singleton
    fun provideApplicationContext(app: Application): Context = app
}