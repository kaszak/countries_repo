package com.example.countries.country

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import com.example.countries.DateFormatter
import com.example.countries.GlideApp
import com.example.countries.R
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_details.*
import javax.inject.Inject

class CountryActivity : DaggerAppCompatActivity(), CountryView {

    private var countryId: Int = INVALID_COUNTRY_ID
    private var seeMoreUrl: String = ""

    @Inject
    lateinit var presenter: CountryPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        title = ""

        presenter.init(this)

        seeMoreBtn.setOnClickListener {
            presenter.onUrl(seeMoreUrl)
        }

        intent?.let {
            countryId = it.getIntExtra(COUNTRY_ID_EXTRA, INVALID_COUNTRY_ID)
        }

        when (countryId) {
            INVALID_COUNTRY_ID -> Log.e(TAG, "Wrong countryId")
            else -> {
                if (DBG) Log.d(TAG, "countryId = $countryId")
                presenter.fetchCountry(countryId)
            }
        }
    }

    override fun onDestroy() {
        presenter.clear()
        super.onDestroy()
    }

    override fun showCountryDetails(countryExt: CountryExtendedViewModel) {
        val country = countryExt.countryViewModel
        title = country.name

        GlideApp.with(this)
                .load(country.pictureUrl)
                .centerCrop()
                .into(detailsPicture)

        detailsDate.text = DateFormatter.formatDate(country.date)
        showDescription(countryExt.description)
        seeMoreUrl = countryExt.seeMoreUrl
    }

    override fun onError() {
        Snackbar.make(detailsMainLayout,
                R.string.details_country_error,
                Snackbar.LENGTH_LONG)
                .show()
    }

    override fun openUrlInBrowser(seeMoreUrl: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(seeMoreUrl)
        startActivity(intent)
    }

    private fun showDescription(description: String) {
        detailsDescription.text = description.substringBefore('\n')
        detailsMoreDescription.text = description.substringAfter('\n', "")
    }

    companion object {
        const val COUNTRY_ID_EXTRA = "countryId"

        private const val INVALID_COUNTRY_ID = -1
        private const val DBG = false

        private val TAG = CountryActivity::class.java.simpleName

        fun start(context: Context, countryId: Int) {
            context.startActivity(
                    Intent(context, CountryActivity::class.java)
                            .putExtra(COUNTRY_ID_EXTRA, countryId))
        }
    }
}