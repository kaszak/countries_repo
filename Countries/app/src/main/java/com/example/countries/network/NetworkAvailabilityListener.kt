package com.example.countries.network

import io.reactivex.Flowable

interface NetworkAvailabilityListener {
    fun getNetworkAvailable(): Flowable<Boolean>
    fun isAvailable(): Boolean
}