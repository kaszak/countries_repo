package com.example.countries.country

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class CountryActivityModule {
    @ContributesAndroidInjector
    abstract fun provideCountryActivity(): CountryActivity
}