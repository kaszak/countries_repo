package com.example.countries.network

import android.content.Context
import android.util.Log
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkAvailabilityManager @Inject constructor(private val appContext: Context) : NetworkAvailabilityListener {

    private val networkAvailable = BehaviorProcessor.create<Boolean>()

    fun start(): Disposable = ReactiveNetwork
            .observeNetworkConnectivity(appContext)
            .flatMapSingle {
                ReactiveNetwork.checkInternetConnectivity().onErrorResumeNext {
                    Single.just(false)
                }
            }
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                    onNext = { networkAvailable.offer(it) },
                    onError = { Log.e(TAG, "RxNetwork observable : $it.message") }
            )

    override fun getNetworkAvailable(): Flowable<Boolean> = networkAvailable

    override fun isAvailable(): Boolean = (networkAvailable.value == true)

    companion object {
        val TAG = NetworkAvailabilityManager::class.java.simpleName
    }
}