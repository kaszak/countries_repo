package com.example.countries.country

import com.example.countries.domain.Mapper
import com.example.countries.domain.country.Country
import java.util.*
import javax.inject.Inject

data class CountryViewModel(val id: Int = -1,
                            val pictureUrl: String = "",
                            val name: String = "",
                            val date: Date = Date()) : Comparable<CountryViewModel> {

    override fun compareTo(other: CountryViewModel): Int = date.compareTo(other.date)
}

class CountryViewModelMapper @Inject constructor() : Mapper<Country, CountryViewModel> {
    override fun map(obj: Country): CountryViewModel {
        return with(obj) {
            CountryViewModel(id, pictureUrl, name, date)
        }
    }
}