package com.example.countries.countries

import android.util.Log
import com.example.countries.country.CountryViewModelMapper
import com.example.countries.domain.countries.GetCountriesUseCase
import com.example.countries.mvp.BasePresenter
import com.example.countries.mvp.View
import com.example.countries.network.NetworkAvailabilityListener
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CountriesPresenter @Inject constructor(private val getCountriesUseCase: GetCountriesUseCase,
                                             private val countryViewModelMapper: CountryViewModelMapper,
                                             private val networkAvailability: NetworkAvailabilityListener) : BasePresenter() {

    private val fetchDisposables = CompositeDisposable()
    private lateinit var mainView: CountriesView

    override fun init(view: View) {
        mainView = view as CountriesView
    }

    fun initNetworkAvailability() {
        networkAvailability
                .getNetworkAvailable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = { netAvailable ->
                            if (netAvailable) getCountries()
                            else mainView.onNoNetworkAvailable()
                        }
                ).addTo(disposables)
    }

    fun getCountries() {
        fetchDisposables.clear()
        networkAvailability.getNetworkAvailable()
                .firstOrError()
                .flatMapMaybe {
                    if (it) {
                        getCountriesUseCase
                                .run()
                                .map { list ->
                                    list.map(countryViewModelMapper::map)
                                }
                                .map { cvmList -> cvmList.sorted() } // cvmList - CountryViewModelList
                                .toMaybe()

                    } else {
                        Maybe.empty()
                    }
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEvent { _, _ -> mainView.showProgress(false) }
                .doOnDispose { mainView.showProgress(false) }
                .doOnSubscribe { mainView.showProgress(true) }
                .subscribeBy(
                        onSuccess = { countries -> mainView.showCountries(countries) },
                        onError = { Log.e(TAG, "getCountries : $it.message") }
                ).addTo(disposables)
                .addTo(fetchDisposables)
    }

    fun onDetailsActivity(countryId: Int) {
        if (networkAvailability.isAvailable())
            mainView.startCountryActivity(countryId)
        else
            mainView.showNoDetailsInfo()
    }

    companion object {
        val TAG = CountriesPresenter::class.java.name
    }
}