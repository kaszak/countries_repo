package com.example.countries.country

import com.example.countries.domain.country.GetCountryUseCase
import com.example.countries.mvp.BasePresenter
import com.example.countries.mvp.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CountryPresenter @Inject constructor(private val getCountryUseCase: GetCountryUseCase,
                                           private val countryExtendedViewModelMapper: CountryExtendedViewModelMapper) : BasePresenter() {

    private lateinit var countryView: CountryView

    override fun init(view: View) {
        countryView = view as CountryView
    }

    fun fetchCountry(countryId: Int) {
        getCountryUseCase
                .run(countryId)
                .map { countryExtendedViewModelMapper.map(it) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(10)
                .subscribeBy(
                        onSuccess = { countryView.showCountryDetails(it) },
                        onError = { countryView.onError() }
                ).addTo(disposables)
    }

    fun onUrl(seeMoreUrl: String) = countryView.openUrlInBrowser(seeMoreUrl)
}