package com.example.countries.countries

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.countries.R
import com.example.countries.country.CountryViewModel
import com.example.countries.country.CountryActivity
import com.google.android.material.snackbar.Snackbar
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.commons.utils.DiffCallback
import com.mikepenz.fastadapter.commons.utils.FastAdapterDiffUtil
import com.mikepenz.fastadapter.listeners.ClickEventHook
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class CountriesActivity : DaggerAppCompatActivity(), CountriesView {

    @Inject
    lateinit var presenter: CountriesPresenter

    private lateinit var fastAdapter: FastAdapter<CountriesItem>

    private val itemAdapter = ItemAdapter<CountriesItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Presenter
        presenter.init(this)

        // Setup layout manager
        countryList.layoutManager = LinearLayoutManager(this)

        // FastAdapter
        fastAdapter = FastAdapter.with(itemAdapter)
        fastAdapter.withEventHook(object : ClickEventHook<CountriesItem>() {

            override fun onBind(viewHolder: RecyclerView.ViewHolder): View? = viewHolder.itemView

            override fun onClick(v: View, position: Int, fastAdapter: FastAdapter<CountriesItem>, item: CountriesItem) {
                presenter.onDetailsActivity(item.country.id)
            }
        })
        countryList.adapter = fastAdapter

        // SwipeRefresh
        swipeRefresh.setOnRefreshListener { presenter.getCountries() }

        // Network availability

        presenter.initNetworkAvailability()

        presenter.getCountries()
    }

    override fun onDestroy() {
        presenter.clear()
        super.onDestroy()
    }

    override fun showProgress(show: Boolean) {
        swipeRefresh.isRefreshing = show
    }

    override fun showCountries(countries: List<CountryViewModel>) {
        // Close refresher animation
        swipeRefresh.isRefreshing = false

        val countryItems = countries.map { CountriesItem(it) }

        FastAdapterDiffUtil.set(itemAdapter, countryItems, object : DiffCallback<CountriesItem> {
            override fun areItemsTheSame(oldItem: CountriesItem, newItem: CountriesItem): Boolean {
                return oldItem.country == newItem.country
            }

            override fun getChangePayload(oldItem: CountriesItem, oldItemPosition: Int, newItem: CountriesItem, newItemPosition: Int): Any? {
                return null
            }

            override fun areContentsTheSame(oldItem: CountriesItem, newItem: CountriesItem): Boolean {
                val oldCountry = oldItem.country
                val newCountry = newItem.country

                return oldCountry.pictureUrl == newCountry.pictureUrl &&
                        oldCountry.name == newCountry.name &&
                        oldCountry.date == newCountry.date
            }
        })
    }

    override fun onError() = showSnackbar(R.string.countries_error)

    override fun startCountryActivity(countryId: Int) = CountryActivity.start(this, countryId)

    override fun onNoNetworkAvailable() {
        swipeRefresh.isRefreshing = false
        showSnackbar(R.string.network_not_available)
    }

    override fun showNoDetailsInfo() = showSnackbar(R.string.details_not_available)

    private fun showSnackbar(stringResId: Int) {
        Snackbar
                .make(countryList, stringResId, Snackbar.LENGTH_LONG)
                .show()
    }
}
