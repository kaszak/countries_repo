package com.example.countries.countries

import com.example.countries.country.CountryViewModel
import com.example.countries.mvp.View

interface CountriesView : View {
    fun showProgress(show: Boolean)
    fun showCountries(countries: List<CountryViewModel>)
    fun startCountryActivity(countryId: Int)
    fun onNoNetworkAvailable()
    fun showNoDetailsInfo()
}
