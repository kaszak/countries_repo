package com.example.countries

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

object DateFormatter {
    private val dateFormatter = SimpleDateFormat("yyyy-MM", Locale.getDefault())

    fun formatDate(date: Date): String = dateFormatter.format(date.time)
}
