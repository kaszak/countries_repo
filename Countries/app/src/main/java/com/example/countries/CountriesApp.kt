package com.example.countries

import com.example.countries.network.NetworkAvailabilityManager
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import javax.inject.Inject

class CountriesApp : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> = DaggerCountriesAppComponent.builder().create(this)

    @Inject
    lateinit var networkManager: NetworkAvailabilityManager

    override fun onCreate() {
        super.onCreate()

        networkManager.start()
    }
}