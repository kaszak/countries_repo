package com.example.countries.mvp

import io.reactivex.disposables.CompositeDisposable

interface View {
    fun onError()
}

abstract class BasePresenter {
    protected val disposables = CompositeDisposable()

    abstract fun init(view: View)

    fun clear() = disposables.clear()
}