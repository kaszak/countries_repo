package com.example.countries.countries

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class CountriesActivityModule {
    @ContributesAndroidInjector
    abstract fun provideCountriesActivity(): CountriesActivity
}