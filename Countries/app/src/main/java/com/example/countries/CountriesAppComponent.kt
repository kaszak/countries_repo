package com.example.countries

import com.example.countries.data.injection.NetworkModule
import com.example.countries.countries.CountriesActivityModule
import com.example.countries.country.CountryActivityModule
import com.example.countries.data.injection.DataModule
import com.example.countries.network.NetworkAvailabilityModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            AndroidSupportInjectionModule::class,
            CountriesActivityModule::class,
            CountryActivityModule::class,
            NetworkModule::class,
            CountriesAppContextModule::class,
            DataModule::class,
            NetworkAvailabilityModule::class
        ])
interface CountriesAppComponent : AndroidInjector<CountriesApp> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<CountriesApp>()
}