package com.example.countries.network

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkAvailabilityModule {

    @Provides
    @Singleton
    fun provideNetworkAvailabilityModule(networkAvailabilityManager: NetworkAvailabilityManager)
            : NetworkAvailabilityListener = networkAvailabilityManager
}