package com.example.countries.country

import com.example.countries.domain.Mapper
import com.example.countries.domain.country.CountryExtended
import javax.inject.Inject

data class CountryExtendedViewModel(val countryViewModel: CountryViewModel,
                                    val description: String = "",
                                    val seeMoreUrl: String = "")

class CountryExtendedViewModelMapper @Inject constructor() : Mapper<CountryExtended, CountryExtendedViewModel> {
    override fun map(obj: CountryExtended): CountryExtendedViewModel {
        return with(obj) {
            CountryExtendedViewModel(
                    with(this.country) { CountryViewModel(id, pictureUrl, name, date) },
                    description,
                    seeMoreUrl
            )
        }
    }
}
