package com.example.countries.countries

import android.view.View
import com.example.countries.DateFormatter
import com.example.countries.GlideApp
import com.example.countries.R
import com.example.countries.country.CountryViewModel
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import kotlinx.android.synthetic.main.country_list_row.view.*

class CountriesItem(val country: CountryViewModel)
    : AbstractItem<CountriesItem, CountriesItem.CountryViewHolder>() {

    override fun getType(): Int = R.id.countryItemId

    override fun getViewHolder(v: View): CountryViewHolder = CountryViewHolder(v)

    override fun getLayoutRes(): Int = R.layout.country_list_row

    class CountryViewHolder(view: View) : FastAdapter.ViewHolder<CountriesItem>(view) {
        private val picture = view.picture
        private val countryName = view.countryName
        private val date = view.date

        override fun unbindView(item: CountriesItem) {}

        override fun bindView(item: CountriesItem, payloads: MutableList<Any>) {
            item.country.also {
                GlideApp.with(itemView)
                        .load(it.pictureUrl)
                        .placeholder(android.R.drawable.ic_menu_gallery)
                        .centerCrop()
                        .into(picture)

                countryName.text = it.name
                date.text = DateFormatter.formatDate(it.date)
            }
        }
    }
}