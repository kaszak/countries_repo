package com.example.countries.country

import com.example.countries.mvp.View

interface CountryView : View {
    fun showCountryDetails(countryExt: CountryExtendedViewModel)
    fun openUrlInBrowser(seeMoreUrl: String)
}