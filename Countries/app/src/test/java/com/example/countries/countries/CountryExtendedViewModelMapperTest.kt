package com.example.countries.countries

import com.example.countries.country.CountryExtendedViewModel
import com.example.countries.country.CountryExtendedViewModelMapper
import com.example.countries.country.CountryViewModel
import com.example.countries.domain.country.Country
import com.example.countries.domain.country.CountryExtended
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.util.*


@RunWith(MockitoJUnitRunner::class)
class CountryExtendedViewModelMapperTest {

    private lateinit var mapper: CountryExtendedViewModelMapper

    @Before
    fun init() {
        mapper = CountryExtendedViewModelMapper()
    }

    @Test
    fun testCountryExtEntityMapper() {
        val input = CountryExtended(
                Country(
                        id = 1002,
                        pictureUrl = "http://serene-mountain-2455.herokuapp.com/images/switzerland.jpg",
                        name = "Switzerland",
                        date = Date(1563181865476)),
                description = "This is description",
                seeMoreUrl = "https://pl.wikipedia.org/wiki/Szwajcaria")

        val expected = CountryExtendedViewModel(
                CountryViewModel(
                        id = 1002,
                        pictureUrl = "http://serene-mountain-2455.herokuapp.com/images/switzerland.jpg",
                        name = "Switzerland",
                        date = Date(1563181865476)),
                description = "This is description",
                seeMoreUrl = "https://pl.wikipedia.org/wiki/Szwajcaria")

        Assert.assertEquals(expected, mapper.map(input))
    }
}