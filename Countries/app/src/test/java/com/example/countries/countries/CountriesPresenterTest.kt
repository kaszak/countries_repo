package com.example.countries.countries

import com.example.countries.country.CountryViewModelMapper
import com.example.countries.domain.countries.GetCountriesUseCase
import com.example.countries.network.NetworkAvailabilityListener
import com.nhaarman.mockito_kotlin.*
import io.github.plastix.rxschedulerrule.RxSchedulerRule
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CountriesPresenterTest {
    @Mock
    private lateinit var getCountriesUseCase: GetCountriesUseCase

    @Mock
    private lateinit var countryViewModelMapper: CountryViewModelMapper

    @Mock
    private lateinit var networkAvailability: NetworkAvailabilityListener

    @Mock
    private lateinit var view: CountriesView

    @Mock
    private lateinit var disposable: Disposable

    @get:Rule
    val rxSchedulers = RxSchedulerRule()

    private lateinit var countriesPresenter: CountriesPresenter

    @Before
    fun setUp() {
        reset(getCountriesUseCase, countryViewModelMapper, networkAvailability, view, disposable)
        countriesPresenter = CountriesPresenter(getCountriesUseCase, countryViewModelMapper, networkAvailability)
        countriesPresenter.init(view)
    }

    @Test
    fun `initNetworkAvailability - network NOT available`() {
        whenever(networkAvailability.getNetworkAvailable()).thenReturn(Flowable.just(false))
        //whenever(networkAvailability.observeNetwork()).thenReturn(disposable)

        countriesPresenter.initNetworkAvailability()

        verify(view).onNoNetworkAvailable()
        verifyNoMoreInteractions(view)
    }

    @Test
    fun `getCountries - network available`() {
        whenever(getCountriesUseCase.run()).thenReturn(Single.just(listOf(mock())))
        whenever(networkAvailability.getNetworkAvailable()).thenReturn(Flowable.just(true))

        countriesPresenter.getCountries()

        verify(view).showCountries(any())
        verify(view, times(2)).showProgress(any())
        verifyNoMoreInteractions(view)
    }

    @Test
    fun `getCountries - network NOT available`() {
        whenever(networkAvailability.getNetworkAvailable()).thenReturn(Flowable.just(false))

        countriesPresenter.getCountries()

        verify(view, times(2)).showProgress(any())
        verifyNoMoreInteractions(view)
    }

    @Test
    fun `getCountries - GetCountriesUseCase error`() {
        whenever(getCountriesUseCase.run()).thenThrow(NullPointerException())
        whenever(networkAvailability.getNetworkAvailable()).thenReturn(Flowable.just(true))

        countriesPresenter.getCountries()
        verify(view, times(2)).showProgress(any())
        verifyNoMoreInteractions(view)
    }

    @Test
    fun `onDetailsActivity - network available`() {
        whenever(networkAvailability.isAvailable()).thenReturn(true)

        countriesPresenter.onDetailsActivity(1)

        verify(view).startCountryActivity(1)
        verifyNoMoreInteractions(view)
    }

    @Test
    fun `onDetailsActivity - network NOT available`() {
        whenever(networkAvailability.isAvailable()).thenReturn(false)

        countriesPresenter.onDetailsActivity(1)

        verify(view).showNoDetailsInfo()
        verifyNoMoreInteractions(view)
    }
}