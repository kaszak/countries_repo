package com.example.countries.country

import com.example.countries.domain.country.GetCountryUseCase
import com.nhaarman.mockito_kotlin.*
import io.github.plastix.rxschedulerrule.RxSchedulerRule
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.lang.IllegalArgumentException
import java.lang.NullPointerException

@RunWith(MockitoJUnitRunner::class)
class CountryPresenterTest {
    @Mock
    private lateinit var getCountryUseCase: GetCountryUseCase

    @Mock
    private lateinit var mapper: CountryExtendedViewModelMapper

    @Mock
    private lateinit var view: CountryView

    private lateinit var countryPresenter: CountryPresenter

    @get:Rule
    val rxSchedulers = RxSchedulerRule()

    @Before
    fun setUp() {
        reset(getCountryUseCase, mapper, view)
        countryPresenter = CountryPresenter(getCountryUseCase, mapper)
        countryPresenter.init(view)
    }

    @Test
    fun `get country success`() {
        whenever(getCountryUseCase.run(anyInt())).thenReturn(Single.just(mock()))
        whenever(mapper.map(any())).thenReturn(mock())

        countryPresenter.fetchCountry(anyInt())

        verify(view).showCountryDetails(any())
        verifyNoMoreInteractions(view)
    }

    @Test
    fun `get country failure`() {
        whenever(getCountryUseCase.run(anyInt())).thenReturn(Single.error(NullPointerException()))

        countryPresenter.fetchCountry(anyInt())

        verify(view).onError()
        verifyNoMoreInteractions(view)
    }

    @Test
    fun `get country mapper failure`() {
        whenever(getCountryUseCase.run(anyInt())).thenReturn(Single.just(mock()))
        whenever(mapper.map(any())).thenThrow(IllegalArgumentException())

        countryPresenter.fetchCountry(anyInt())

        verify(view).onError()
        verifyNoMoreInteractions(view)
    }
}