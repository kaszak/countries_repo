package com.example.countries.data.country

import com.nhaarman.mockito_kotlin.*
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CountryDataRepositoryTest {
    private lateinit var dataRepo: CountryDataRepository

    @Mock
    private lateinit var countryService: CountryService

    @Mock
    private lateinit var countryEntityMapper: CountryEntityMapper

    @Mock
    private lateinit var countryExtEntityMapper: CountryExtendedEntityMapper

    @Before
    fun setUp() {
        reset(countryService, countryEntityMapper, countryExtEntityMapper)
        dataRepo = CountryDataRepository(countryService, countryEntityMapper, countryExtEntityMapper)
    }

    @Test
    fun testGetCountrySuccess() {
        whenever(countryService.fetchCountry(anyInt())).thenReturn(Single.just(mock()))
        whenever(countryExtEntityMapper.map(any())).thenReturn(mock())

        dataRepo.getCountry(anyInt()).test()
                .assertNoErrors()
                .assertComplete()
                .assertValueCount(1)

        verify(countryService).fetchCountry(anyInt())
        verifyNoMoreInteractions(countryService)
        verify(countryExtEntityMapper).map(any())
        verifyNoMoreInteractions(countryExtEntityMapper)
    }

    @Test
    fun testGetCountryFailure() {
        whenever(countryService.fetchCountry(anyInt())).thenReturn(Single.just(mock()))
        whenever(countryExtEntityMapper.map(any())).thenThrow(NullPointerException())

        dataRepo.getCountry(anyInt()).test()
                .assertError(NullPointerException::class.java)
                .assertNotComplete()
                .assertNoValues()

        verify(countryService).fetchCountry(anyInt())
        verifyNoMoreInteractions(countryService)
        verify(countryExtEntityMapper).map(any())
        verifyNoMoreInteractions(countryExtEntityMapper)
    }

    @Test
    fun testGetCountriesSuccess() {
        whenever(countryService.fetchCountries()).thenReturn(Single.just(listOf(mock())))
        whenever(countryEntityMapper.map(any())).thenReturn(mock())

        dataRepo.getCountries().test()
                .assertNoErrors()
                .assertComplete()
                .assertValueCount(1)

        verify(countryService).fetchCountries()
        verifyNoMoreInteractions(countryService)
        verify(countryEntityMapper).map(any())
        verifyNoMoreInteractions(countryEntityMapper)
    }

    @Test
    fun testGetCountriesFailure() {
        whenever(countryService.fetchCountries()).thenReturn(Single.just(listOf(mock())))
        whenever(countryEntityMapper.map(any())).thenThrow(NullPointerException())

        dataRepo.getCountries().test()
                .assertError(NullPointerException::class.java)
                .assertNotComplete()
                .assertNoValues()

        verify(countryService).fetchCountries()
        verifyNoMoreInteractions(countryService)
        verify(countryEntityMapper).map(any())
        verifyNoMoreInteractions(countryEntityMapper)
    }
}