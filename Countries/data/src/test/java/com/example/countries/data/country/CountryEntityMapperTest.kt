package com.example.countries.data.country

import com.example.countries.domain.country.Country
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class CountryEntityMapperTest {
    private lateinit var mapper: CountryEntityMapper

    @Before
    fun setUp() {
        mapper = CountryEntityMapper()
    }

    @Test
    fun testMapping() {
        val input = CountryEntity(
                id = 1001,
                pictureUrl = "http://serene-mountain-2455.herokuapp.com/images/switzerland.jpg",
                name = "Switzerland",
                date = Date(1563181865476))

        val expected = Country(
                id = 1001,
                pictureUrl = "http://serene-mountain-2455.herokuapp.com/images/switzerland.jpg",
                name = "Switzerland",
                date = Date(1563181865476))

        Assert.assertEquals(expected, mapper.map(input))
    }
}