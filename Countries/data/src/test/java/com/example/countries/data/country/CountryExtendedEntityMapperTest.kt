package com.example.countries.data.country

import com.example.countries.domain.country.Country
import com.example.countries.domain.country.CountryExtended
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class CountryExtendedEntityMapperTest {
    private lateinit var mapper: CountryExtendedEntityMapper

    @Before
    fun init() {
        mapper = CountryExtendedEntityMapper()
    }

    @Test
    fun testCountryExtEntityMapper() {
        val input = CountryExtendedEntity(
                id = 1002,
                pictureUrl = "http://serene-mountain-2455.herokuapp.com/images/switzerland.jpg",
                name = "Switzerland",
                date = Date(1563181865476),
                description = "This is description",
                seeMoreUrl = "https://pl.wikipedia.org/wiki/Szwajcaria")

        val expected = CountryExtended(
                Country(
                        id = 1002,
                        pictureUrl = "http://serene-mountain-2455.herokuapp.com/images/switzerland.jpg",
                        name = "Switzerland",
                        date = Date(1563181865476)),
                description = "This is description",
                seeMoreUrl = "https://pl.wikipedia.org/wiki/Szwajcaria")

        Assert.assertEquals(expected, mapper.map(input))
    }
}