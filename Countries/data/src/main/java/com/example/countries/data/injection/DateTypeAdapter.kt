package com.example.countries.data.injection

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import java.util.Date

class DateTypeAdapter : TypeAdapter<Date>() {
    override fun write(out: JsonWriter?, value: Date?) {
    }

    override fun read(input: JsonReader?): Date {
        val date = Date()
        input?.let { date.time = input.nextLong() * 1000L }
        return date
    }
}