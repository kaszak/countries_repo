package com.example.countries.data.country

import com.example.countries.domain.Mapper
import com.example.countries.domain.country.Country
import com.example.countries.domain.country.CountryExtended
import com.google.gson.annotations.SerializedName
import java.util.*
import javax.inject.Inject

data class CountryExtendedEntity(
        @SerializedName("id") val id: Int = -1,
        @SerializedName("picture_url") val pictureUrl: String = "",
        @SerializedName("name") val name: String = "",
        @SerializedName("date") val date: Date = Date(),
        @SerializedName("description") val description: String = "",
        @SerializedName("see_more_url") val seeMoreUrl: String = "")

class CountryExtendedEntityMapper @Inject constructor() : Mapper<CountryExtendedEntity, CountryExtended> {
    override fun map(obj: CountryExtendedEntity): CountryExtended {
        return with(obj) {
            CountryExtended(
                    Country(id, pictureUrl, name, date),
                    description,
                    seeMoreUrl
            )
        }
    }
}