package com.example.countries.data.country

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface CountryService {

    @GET("countries")
    fun fetchCountries(): Single<List<CountryEntity>>

    @GET("countries/{id}")
    fun fetchCountry(@Path("id") countryId: Int): Single<CountryExtendedEntity>
}