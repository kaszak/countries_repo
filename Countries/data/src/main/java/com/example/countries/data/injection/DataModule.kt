package com.example.countries.data.injection

import com.example.countries.data.country.CountryDataRepository
import com.example.countries.domain.country.CountryRepository
import dagger.Binds
import dagger.Module

@Module
abstract class DataModule {
    @Binds
    abstract fun provideCountryRepository(countryDataRepository: CountryDataRepository): CountryRepository
}