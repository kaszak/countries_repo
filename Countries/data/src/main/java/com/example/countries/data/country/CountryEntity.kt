package com.example.countries.data.country

import com.example.countries.domain.Mapper
import com.example.countries.domain.country.Country
import com.google.gson.annotations.SerializedName
import java.util.*
import javax.inject.Inject

data class CountryEntity(
        @SerializedName("id") val id: Int = -1,
        @SerializedName("picture_url") val pictureUrl: String = "",
        @SerializedName("name") val name: String = "",
        @SerializedName("date") val date: Date = Date())

class CountryEntityMapper @Inject constructor() : Mapper<CountryEntity, Country> {
    override fun map(obj: CountryEntity): Country {
        return with(obj) {
            Country(id, pictureUrl, name, date)
        }
    }
}