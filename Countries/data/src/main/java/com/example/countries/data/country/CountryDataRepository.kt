package com.example.countries.data.country

import com.example.countries.domain.country.Country
import com.example.countries.domain.country.CountryExtended
import com.example.countries.domain.country.CountryRepository
import io.reactivex.Single
import javax.inject.Inject

class CountryDataRepository @Inject constructor(private val countryService: CountryService,
                                                private val countryEntityMapper: CountryEntityMapper,
                                                private val countryExtendedEntityMapper: CountryExtendedEntityMapper) : CountryRepository {
    
    override fun getCountry(countryId: Int): Single<CountryExtended> {
        return countryService.fetchCountry(countryId).map { countryExtEntity ->
            countryExtendedEntityMapper.map(countryExtEntity)
        }
    }

    override fun getCountries(): Single<List<Country>> {
        return countryService.fetchCountries().map { list ->
            list.map { countryEntity ->
                countryEntityMapper.map(countryEntity)
            }
        }
    }
}