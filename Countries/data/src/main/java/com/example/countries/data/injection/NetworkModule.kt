package com.example.countries.data.injection

import com.example.countries.data.country.CountryService
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    fun providesRetrofit(@Named("baseUrl") baseUrl: String, gson: Gson): Retrofit {
        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()
    }

    @Provides
    fun providesGson(dateTypeAdapter: DateTypeAdapter): Gson {
        return GsonBuilder()
                .setLenient()
                .registerTypeAdapter(Date::class.java, dateTypeAdapter)
                .create()
    }

    @Provides
    fun providesDateTypeAdapter(): DateTypeAdapter = DateTypeAdapter()

    @Provides
    @Named("baseUrl")
    fun providesBaseUrl(): String = "https://serene-mountain-2455.herokuapp.com/"

    @Provides
    @Singleton
    fun providesCountries(retrofit: Retrofit): CountryService = retrofit.create(CountryService::class.java)
}